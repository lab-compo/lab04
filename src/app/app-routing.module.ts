import { NgModule} from '@angular/core';
import {RouterModule, Routes } from '@angular/router';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsComponent } from './students/list/students.component';
import { StudentsViewComponent } from './students/view/students.view.component';

const appRoutes: Routes = [
    {path: 'view', component: StudentsViewComponent },
    {path: 'add', component: StudentsAddComponent },
    {path: 'list', component: StudentsComponent },
];

@NgModule ({
        imports : [
            RouterModule.forRoot(appRoutes)
        ],
        exports: [
            RouterModule
        ]
    })
export class AppRoutingModule {

}